<h3>What excites you about this course?</h3>
<p> I am excited to learn better data management skills. I work with data (although not usually very large ones) in the lab most of the time and I think having more data management skills outside of what I currently do on R will be very helpful. </p>
<h3>What worries you about this course?</h3>
<p> Although I have some experience with python and using the terminal from previous classes, I am still not as comfortable with the technical skills I'll need for this course as I'd like to be. I worry that it will take me quite a while to figure out how to execute what I want, which can be more frustrating than conceptually not understanding the material </p>
<h3>What one skill are you most hoping to learn and why?</h3>
<p> I am most excited about learning new visualization techniques. My favorite part of research is getting to look at the data after all the collection and preprocessing has taken place and I enjoy aquiring new tools to help improve this step in the process.  </p>
<h3>What role do you think data management will play in your career?</h3>
<p> I am going into medical research in some capacity so I think undersanding and working with big data sets (especially as high thoroughput assays like RNAseq come to dominate a lot of neuroscience work) will be increasingly important to my career. </p>
<h3>What type of dataset are you hoping to work with this quarter and why?</h3>
<p> I would like to work with a dataset related to the brain and to Alzheimer's disease if possible. I find that I enjoy classes when they related to my personal interests so I would be great if I was able to find a dataset that fit these. </p>
